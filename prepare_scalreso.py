from __future__ import print_function
import os
import importlib.util
import sys
import math
import itertools
import json
from array import array
import ctypes
import ROOT
import tdrstyle
import CMS_lumi

from dataset_allowed_definitions import get_data_mc_sub_eras
from muon_definitions import (get_full_name, get_eff_name,
                              get_bin_name,
                              get_extended_eff_name,
                              get_variables_name)
from registry import registry

ROOT.gROOT.SetBatch()
ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 1001;")
tdrstyle.setTDRStyle()


def computeEff(n1, n2, e1, e2):
    eff = n1 / (n1 + n2)
    err = 1 / (n1 + n2) * math.sqrt(
        e1 * e1 * n2 * n2 + e2 * e2 * n1 * n1) / (n1 + n2)
    return eff, err


# xiaohu: getEff(forMC) is removed as it is cutandcount
#         getDataEff -> getFitted, use only parameters (mean,sigma) out fits
def getFitted(binName, fname, shift=None, resonance='Z', etype='escale'):
    
    # window shift is not in use, since we do not cut a window and count as done for eff

    try:
        tfile = ROOT.TFile(fname, 'read')
        fitresP = tfile.Get('{}_resP'.format(binName))
        fitresF = tfile.Get('{}_resF'.format(binName))

        if etype == 'escale':
          pP = fitresP.floatParsFinal().find('meanP')
          pF = fitresF.floatParsFinal().find('meanF')
        elif etype == 'ereso':
          pP = fitresP.floatParsFinal().find('sigmaP')
          pF = fitresF.floatParsFinal().find('sigmaF')
        else:
          print('getEScaleReso :: etype error')
          exit(1)

        vP = pP.getVal()
        vF = pF.getVal()
        eP = pP.getError()
        eF = pF.getError()

        tfile.Close()
        return vP, eP, vF, eF

    except Exception as e:
        print('Exception for getEScaleReso', binName)
        print(e)
        # raise e
        return 1., 0., 1., 0.


def getEScalReso(binName, datafname, mcfname, shift=None, resonance='Z', etype='escale'):

    # shift is not in use atm, see getFitted
    # etype, see getFitted

    vPdata, ePdata, vFdata, eFdata = getFitted(binName, datafname, shift, resonance, etype)
    vPmc, ePmc, vFmc, eFmc = getFitted(binName, mcfname, shift, resonance, etype)
    vdiff = vPdata / vPmc
    ediff = vdiff * ((ePdata/vPdata)**2 + (ePmc/vPmc)**2)**0.5
    # only use Pass and drop Fail atm
    return vdiff, ediff, vPdata, ePdata, vPmc, ePmc

def getSyst(binName, datafname, mcfname, fitTypes, shiftTypes, resonance='Z', etype='escale'):
    sf, sf_err, dataEff, dataErr, mcEff, mcErr = getSF(binName, fname, resonance=resonance)
    vdiff, ediff, vdata, edata, vmc, emc = getEScalReso(binName,\
        datafname, mcfname, shift=None, resonance=resonance, etype=etype)
    # modified till this line atm

    syst = {}
    for isyst in fitTypes:
        systfname = fname.replace('Nominal', isyst)
        # sf, sf_err, dataEff, dataErr, mcEff, mcErr
        tmp = getSF(binName, systfname, isyst, resonance=resonance)
        syst[isyst] = {
            'sf': tmp[0],
            'err': abs(tmp[0]-sf),
            'dataEff': tmp[2],
            'dataErr': abs(tmp[2]-dataEff),
            'mcEff': tmp[4],
            'mcErr': abs(tmp[4]-mcEff),
        }

    for isyst in shiftTypes:
        systUpfname = fname.replace('Nominal', isyst+'Up')
        systDnfname = fname.replace('Nominal', isyst+'Down')
        # sf, sf_err, dataEff, dataErr, mcEff, mcErr
        tmpUp = getSF(binName, systUpfname, isyst+'Up', resonance=resonance)
        tmpDn = getSF(binName, systDnfname, isyst+'Down', resonance=resonance)
        tmp = [
            (tmpUp[0]+tmpDn[0])/2,
            (abs(tmpUp[0]-sf)+abs(tmpDn[0]-sf))/2,
            (tmpUp[2]+tmpDn[2])/2,
            (abs(tmpUp[2]-dataEff)+abs(tmpDn[2]-dataEff))/2,
            (tmpUp[4]+tmpDn[4])/2,
            (abs(tmpUp[4]-mcEff)+abs(tmpDn[4]-mcEff))/2,
        ]
        syst[isyst] = {
            'sf': tmp[0],
            'err': tmp[1],
            'dataEff': tmp[2],
            'dataErr': tmp[3],
            'mcEff': tmp[4],
            'mcErr': tmp[5],
        }
        syst[isyst+'Up'] = {
            'sf': tmpUp[0],
            'err': abs(tmpUp[0]-sf),
            'dataEff': tmpUp[2],
            'dataErr': abs(tmpUp[2]-dataEff),
            'mcEff': tmpUp[4],
            'mcErr': abs(tmpUp[4]-mcEff),
        }
        syst[isyst+'Down'] = {
            'sf': tmpDn[0],
            'err': abs(tmpDn[0]-sf),
            'dataEff': tmpDn[2],
            'dataErr': abs(tmpDn[2]-dataEff),
            'mcEff': tmpDn[4],
            'mcErr': abs(tmpDn[4]-mcEff),
        }

    return syst


# etype can be escale or ereso for energy scale and resolution respectively
def prepare(baseDir, particle, probe, resonance, era,
            config, num, denom, variableLabels, lumi,
            skipPlots=False, cutAndCount=False, etype='escale'):

    if etype == 'escale':
      print('Preparing energy scale plots')
    elif etype == 'ereso':
      print('Preparing energy resolution plots')
    else:
      print('Preparing cannot start as this etype is not supported: {}'.format(etype))
      exit(1)

    hists = {}

    ## retrieve basic config info etc.
    effType = config.type() if 'type' in config else ''
    effName = get_eff_name(num, denom)
    extEffName = get_extended_eff_name(num, denom, variableLabels)
    binning = config.binning()
    dataSubEra, mcSubEra = get_data_mc_sub_eras(resonance, era)

    ## syst list
    # xiaohu: invalid
    systList = config.get('systematics',
                          {x: {'fitTypes': [],
                               'shiftTypes': []}
                           for x in ['SF', 'dataEff', 'mcEff']})

    def get_variable_name_pretty(variableLabel):
        variables = config.variables()
        return variables.get(variableLabel, {}).get('pretty', variableLabel)

    ## create output histograms
    # hist dim, e.g. 2D: ["abseta", "pt"],
    nVars = len(variableLabels)
    if nVars == 1:
        THX = ROOT.TH1F
    elif nVars == 2:
        THX = ROOT.TH2F
    elif nVars == 3:
        THX = ROOT.TH3F
    else:
        raise NotImplementedError(
            'More than 3 dimensions are not supported for scale factors'
        )
    
    # hist binning 
    hargs = [extEffName, extEffName]
    for variableLabel in variableLabels:
        hargs += [len(binning[variableLabel]) - 1,
                  array('d', binning[variableLabel])]
    hist = THX(*hargs)
    axes = [hist.GetXaxis(), hist.GetYaxis(), hist.GetZaxis()]
    for vi, variableLabel in enumerate(variableLabels):
        axes[vi].SetTitle(get_variable_name_pretty(variableLabel))
    if nVars == 1:
        hist.GetYaxis().SetTitle('rel. escale (data/mc)')
    if nVars == 2:
        hist.SetOption('colz')
        hist.GetZaxis().SetTitle('rel. escale (data/mc)')
    hist_stat = hist.Clone(extEffName+'_stat')
    hist_syst = hist.Clone(extEffName+'_syst')
    histList_syst = {
        'combined_syst': hist.Clone(extEffName+'_combined_syst'),
    }
    if nVars == 2:
        histList_syst['combined_syst'].GetZaxis().SetTitle('Uncertainty')

    if etype == 'escale':
        etitle = 'Mass peak'
        reltitle = 'Rel. escale (data/mc)'
    elif etype == 'ereso':
        etitle = 'Mass resolution'
        reltitle = 'Rel. ereso (data/mc)'
    else:
       pass
    hist_dataEff = hist.Clone(extEffName+'_'+etype+'Data')
    if nVars == 1:
        hist_dataEff.GetYaxis().SetTitle(etitle)
    if nVars == 2:
        hist_dataEff.GetZaxis().SetTitle(etitle)
    hist_dataEff_stat = hist_dataEff.Clone(extEffName+'_'+etype+'Data_stat')
    hist_dataEff_syst = hist_dataEff.Clone(extEffName+'_'+etype+'Data_syst')
    histList_dataEff_syst = {
        'combined_syst': hist_dataEff.Clone(
            extEffName+'_'+etype+'Data_combined_syst'),
    }
    if nVars == 2:
        histList_dataEff_syst['combined_syst'].GetZaxis().SetTitle('Uncertainty')
    hist_mcEff = hist_dataEff.Clone(extEffName+'_'+etype+'MC')
    hist_mcEff_stat = hist_dataEff.Clone(extEffName+'_'+etype+'MC_stat')
    hist_mcEff_syst = hist_dataEff.Clone(extEffName+'_'+etype+'MC_syst')
    histList_mcEff_syst = {
        'combined_syst': hist_dataEff.Clone(
            extEffName+'_'+etype+'MC_combined_syst'),
    }
    if nVars == 2:
        histList_mcEff_syst['combined_syst'].GetZaxis().SetTitle('Uncertainty')

    # the individual systematics
    # xiaohu: invalid
    for iSyst in itertools.chain(systList['SF']['fitTypes'],
                                 systList['SF']['shiftTypes']):
        histList_syst[iSyst] = hist.Clone(extEffName+'_'+iSyst)
        histList_syst[iSyst+'_syst'] = hist.Clone(extEffName+'_'+iSyst+'_syst')
        if nVars == 2:
            histList_syst[iSyst+'_syst'].GetZaxis().SetTitle('Uncertainty')
    for iSyst in itertools.chain(systList['dataEff']['fitTypes'],
                                 systList['dataEff']['shiftTypes']):
        histList_dataEff_syst[iSyst] = hist_dataEff.Clone(extEffName+'_'+iSyst)
        histList_dataEff_syst[iSyst+'_syst'] = hist_dataEff.Clone(
            extEffName+'_'+iSyst+'_syst')
        if nVars == 2:
            histList_dataEff_syst[iSyst+'_syst'].GetZaxis().SetTitle('Uncertainty')
    for iSyst in itertools.chain(systList['mcEff']['fitTypes'],
                                 systList['mcEff']['shiftTypes']):
        histList_mcEff_syst[iSyst] = hist_mcEff.Clone(extEffName+'_'+iSyst)
        histList_mcEff_syst[iSyst+'_syst'] = hist_mcEff.Clone(
            extEffName+'_'+iSyst+'_syst')
        if nVars == 2:
            histList_mcEff_syst[iSyst+'_syst'].GetZaxis().SetTitle('Uncertainty')

    varName = get_variables_name(variableLabels)

    # iterate through the bin indices
    # this does nested for loops of the N-D binning (e.g. pt, eta)
    # binning starts at 1 (0 is underflow), same as ROOT
    indices = [list(range(1, len(binning[variableLabel])))
               for variableLabel in variableLabels]
    output = {effName: {varName: {}}}
    all_systematics = {}
    for index in itertools.product(*indices):
        binName = get_full_name(num, denom, variableLabels, index)
        subVarKeys = [
            '{}:[{},{}]'.format(
                variableLabels[i],
                binning[variableLabels[i]][ind-1],
                binning[variableLabels[i]][ind]
            ) for i, ind in enumerate(index)
        ]
        _out = output[effName][varName]

        # add binning definitions
        _out['binning'] = [
            {
                'variable': vl,
                'binning': binning[vl].tolist(),
            }
            for vl in variableLabels
        ]

        for subVarKey in subVarKeys:
            if subVarKey not in _out:
                _out[subVarKey] = {}
            _out = _out[subVarKey]

        # the fitted distributions
        fitType = 'Nominal'
        dataFNameFit = os.path.join(baseDir, 'fits_data',
                                    particle, probe,
                                    resonance, era,
                                    fitType, effName,
                                    binName + '.root')
        mcFNameFit = os.path.join(baseDir, 'fits_mc',
                                    particle, probe,
                                    resonance, era,
                                    fitType, effName,
                                    binName + '.root')
        vdiff, ediff, vdata, edata, vmc, emc= getEScalReso(binName,\
            dataFNameFit, mcFNameFit, shift=None, resonance=resonance, etype=etype)

        fitTypes = set(systList['SF']['fitTypes']
                       + systList['dataEff']['fitTypes']
                       + systList['mcEff']['fitTypes'])
        shiftTypes = set(systList['SF']['shiftTypes']
                         + systList['dataEff']['shiftTypes']
                         + systList['mcEff']['shiftTypes'])

        # TODO
        #sf_syst = getSyst(binName, dataFNameFit, mcFNameFit
        #                      fitTypes, shiftTypes, resonance=resonance, etype=etype)

        #combined_syst = {}
        #for kind in ['SF', 'dataEff', 'mcEff']:
        #    combined_syst[kind] = 0
        #    errKey = 'err'
        #    if kind == 'dataEff':
        #        errKey = 'dataErr'
        #    if kind == 'mcEff':
        #        errKey = 'mcErr'
        #    for t in itertools.chain(systList[kind]['fitTypes'],
        #                             systList[kind]['shiftTypes']):
        #        combined_syst[kind] += sf_syst[t][errKey]**2
        #    combined_syst[kind] = combined_syst[kind]**0.5

        #sf_err = (sf_stat**2 + combined_syst['SF']**2)**0.5
        #dataErr = (dataStat**2 + combined_syst['dataEff']**2)**0.5
        #mcErr = (mcStat**2 + combined_syst['mcEff']**2)**0.5
        #_out['value'] = sf
        #_out['stat'] = sf_stat
        #_out['syst'] = combined_syst['SF']
        #for s in itertools.chain(systList['SF']['fitTypes'],
        #                         systList['SF']['shiftTypes']):
        #    _out[s] = sf_syst[s]['err']

        ## copy systs for later schema
        #all_systematics[index] = _out.copy()

        def set_bin(hist, index, val, err):
            index = list(index)
            val_args = index + [val]
            err_args = index + [err]
            hist.SetBinContent(*val_args)
            if err >= 0:
                hist.SetBinError(*err_args)

        set_bin(hist, index, vdiff, ediff) # this should be total error, but use stat err atm TODO
        set_bin(hist_stat, index, vdiff, ediff)  # stat err
        #set_bin(hist_syst, index, sf, combined_syst['SF'])
        #set_bin(histList_syst['combined_syst'], index,
        #        combined_syst['SF'], -1)
        set_bin(hist_dataEff, index, vdata, edata) # this should be total error, but use stat err atm TODO
        set_bin(hist_dataEff_stat, index, vdata, edata) # stat err
        #set_bin(hist_dataEff_syst, index, dataEff, combined_syst['dataEff'])
        #set_bin(histList_dataEff_syst['combined_syst'], index,
        #        combined_syst['dataEff'], -1)
        set_bin(hist_mcEff, index, vmc, emc) # this should be total error, but use stat err atm TODO
        set_bin(hist_mcEff_stat, index, vmc, emc) # stat err
        #set_bin(hist_mcEff_syst, index, mcEff, combined_syst['mcEff'])
        #set_bin(histList_mcEff_syst['combined_syst'], index,
        #        combined_syst['mcEff'], -1)
        #for iKey in sf_syst.keys():
        #    if iKey in histList_syst:
        #        set_bin(histList_syst[iKey], index,
        #                sf_syst[iKey]['sf'], sf_syst[iKey]['err'])
        #        set_bin(histList_syst[iKey+'_syst'], index,
        #                sf_syst[iKey]['err'], -1)

        #    if iKey in histList_dataEff_syst:
        #        set_bin(histList_dataEff_syst[iKey], index,
        #                sf_syst[iKey]['dataEff'], sf_syst[iKey]['dataErr'])
        #        set_bin(histList_dataEff_syst[iKey+'_syst'], index,
        #                sf_syst[iKey]['dataErr'], -1)

        #    if iKey in histList_mcEff_syst:
        #        set_bin(histList_mcEff_syst[iKey], index,
        #                sf_syst[iKey]['mcEff'], sf_syst[iKey]['mcErr'])
        #        set_bin(histList_mcEff_syst[iKey+'_syst'], index,
        #                sf_syst[iKey]['mcErr'], -1)

    hists[extEffName] = hist
    hists[extEffName+'_stat'] = hist_stat
    hists[extEffName+'_syst'] = hist_syst
    hists[extEffName+'_'+etype+'Data'] = hist_dataEff
    hists[extEffName+'_'+etype+'Data_stat'] = hist_dataEff_stat
    hists[extEffName+'_'+etype+'Data_syst'] = hist_dataEff_syst
    hists[extEffName+'_'+etype+'MC'] = hist_mcEff
    hists[extEffName+'_'+etype+'MC_stat'] = hist_mcEff_stat
    hists[extEffName+'_'+etype+'MC_syst'] = hist_mcEff_syst
    for iKey in histList_syst.keys():
        hname = extEffName+'_'+iKey
        hists[hname] = histList_syst[iKey]
    for iKey in histList_dataEff_syst.keys():
        hname = extEffName+'_'+etype+'Data_'+iKey
        hists[hname] = histList_dataEff_syst[iKey]
    for iKey in histList_mcEff_syst.keys():
        hname = extEffName+'_'+etype+'MC_'+iKey
        hists[hname] = histList_mcEff_syst[iKey]

    ### [1/2] save energy scale and resolution (for 2D or higher)
    plotDir = os.path.join(baseDir, 'plots',
                           particle, probe,
                           resonance, era,
                           effName, etype+'_2d')
    os.makedirs(plotDir, exist_ok=True)

    effDir = os.path.join(baseDir, etype,
                          particle, probe,
                          resonance, era,
                          effName)
    os.makedirs(effDir, exist_ok=True)
    effPath = os.path.join(effDir, extEffName)

    # JSON format
    with open('{}.json'.format(effPath), 'w') as f:
        f.write(json.dumps(output, indent=4, sort_keys=True))

    # not in use for energy scale and reso
    ## Now build the new xPOG schema v1 if correctionlib and pydantic installed

    #schemav1 = None
    #libname = 'correctionlib.schemav1'
    #if libname in sys.modules:
    #    schemav1 = sys.modules[libname]
    #elif importlib.util.find_spec('schemav1', 'correctionlib') is not None:
    #    spec = importlib.util.find_spec('schemav1', 'correctionlib')
    #    schemav1 = importlib.util.module_from_spec(spec)
    #    sys.modules[libname] = schemav1
    #    spec.loader.exec_module(schemav1)

    #if schemav1 is not None:

    #    def build_schema(dim, index):
    #        # If we reach recursion bottom, build and return the systematics node
    #        if dim == len(variableLabels) + 1:
    #            keys, content = [], []
    #            for syst, value in all_systematics[index].items():
    #                keys.append(syst)
    #                content.append(value) 
    #            return schemav1.Category.parse_obj({
    #                "nodetype": "category",
    #                "keys": keys,
    #                "content": content
    #            })
    #        # If not, build a binning node
    #        edges = list(map(float, binning[variableLabels[dim-1]]))
    #        content = [build_schema(dim+1, tuple(list(index)[0:dim-1]+[i]+list(index)[dim:])) for i in indices[dim-1]]
    #        return schemav1.Binning.parse_obj({
    #            "nodetype": "binning",
    #            "edges": edges,
    #            "content": content
    #        })

    #    inputs = [{"name": vl, "type": "real"} for vl in variableLabels]
    #    inputs += [{"name": "uncertainties", "type": "string"}]

    #    corr = schemav1.Correction.parse_obj({
    #            "version": 1,
    #            "name": effName,
    #            "description": effName,
    #            "inputs": inputs,
    #            "output": {"name": "weight", "type": "real"},
    #            "data": build_schema(1, tuple([1]*len(variableLabels)))
    #    })
    #    cset = schemav1.CorrectionSet.parse_obj({
    #        "schema_version": 1,
    #        "corrections": [corr]
    #    })

    #    # Write out schema json
    #    with open('{}_schemaV1.json'.format(effPath), "w") as fout:
    #        fout.write(cset.json(exclude_unset=True, indent=4))

    #else:
    #    print("Warning: correctionlib not installed. Not producing schema jsons.")

    # ROOT histogram format
    tfile = ROOT.TFile.Open('{}.root'.format(effPath), 'recreate')
    for h in sorted(hists):
        hists[h].Write(h)

        if skipPlots:
            continue

        def setLog(canvas, hist, thr = 110.):
            if hist.GetXaxis().GetBinLowEdge(hist.GetXaxis().GetNbins()) > thr:
                hist.GetXaxis().SetMoreLogLabels()
                canvas.SetLogx()
            if hist.GetYaxis().GetBinLowEdge(hist.GetYaxis().GetNbins()) > thr:
                hist.GetYaxis().SetMoreLogLabels()
                canvas.SetLogy()
            if hist.GetZaxis().GetBinLowEdge(hist.GetZaxis().GetNbins()) > thr:
                canvas.SetLogz()

        if nVars == 2:
            cName = 'c' + h
            canvas = ROOT.TCanvas(cName, cName, 1000, 800)
            ROOT.gStyle.SetPaintTextFormat("5.3f")
            canvas.SetRightMargin(0.24)
            hists[h].Draw('colz text')
            plotPath = os.path.join(plotDir, h)
            canvas.Modified()
            canvas.Update()

            CMS_lumi.cmsText = 'CMS'
            CMS_lumi.writeExtraText = True
            CMS_lumi.extraText = 'Preliminary'
            CMS_lumi.lumi_13TeV = "%0.1f fb^{-1}" % (lumi)
            CMS_lumi.CMS_lumi(canvas, 4, 0)

            if effType == 'trig':
                setLog(canvas, hists[h])

            canvas.Print('{}.png'.format(plotPath))
            canvas.Print('{}.pdf'.format(plotPath))
            canvas.Print('{}.root'.format(plotPath))

        elif nVars == 3:
            axes = [hists[h].GetXaxis(),
                    hists[h].GetYaxis(),
                    hists[h].GetZaxis()]
            axislabels = ['x', 'y', 'z']

            def zAxisTitle(effName):
                for iSyst in itertools.chain(systList['SF']['fitTypes'],
                                             systList['SF']['shiftTypes'],
                                             systList['dataEff']['fitTypes'],
                                             systList['dataEff']['shiftTypes'],
                                             systList['mcEff']['fitTypes'],
                                             systList['mcEff']['shiftTypes']):
                    if effName.endswith(iSyst+'_syst'):
                        return 'Uncertainty'

                if effName.endswith('combined_syst'):
                    return 'Uncertainty'
                elif 'efficiency' in effName:
                    return 'Efficiency'
                else:
                    return 'Scalefactor'

            for vi, variableLabel in enumerate(variableLabels):
                # if there are more than 2 bins, skip this variable
                if len(binning[variableLabel]) > 3:
                    continue

                projOpt = 'zyxe'.replace(axislabels[vi], '')
                for ibin in range(1, len(binning[variableLabel])):
                    axes[vi].SetRange(ibin, ibin)
                    projEffName = h.replace(variableLabel, variableLabel+'_{}'.format(ibin))
                    hist_proj = hists[h].Project3D(projOpt).Clone(projEffName)

                    hist_proj.GetZaxis().SetTitle(zAxisTitle(projEffName))

                    cName = 'c' + projEffName
                    canvas = ROOT.TCanvas(cName, cName, 1000, 800)
                    ROOT.gStyle.SetPaintTextFormat("5.3f")
                    canvas.SetRightMargin(0.24)
                    hist_proj.Draw('colz text')
                    plotPath = os.path.join(plotDir, projEffName)
                    canvas.Modified()
                    canvas.Update()

                    CMS_lumi.cmsText = 'CMS'
                    CMS_lumi.writeExtraText = True
                    CMS_lumi.extraText = 'Preliminary'
                    CMS_lumi.lumi_13TeV = "%0.1f fb^{-1}" % (lumi)
                    CMS_lumi.CMS_lumi(canvas, 4, 0)

                    if effType == 'trig':
                        setLog(canvas, hist_proj)

                    canvas.Print('{}.png'.format(plotPath))
                    canvas.Print('{}.pdf'.format(plotPath))
                    canvas.Print('{}.root'.format(plotPath))

    tfile.Close()

    # xiaohu: till now all rel. escale, ereso, signal peak, reso
    #         are saved to root and plots for data and MC respectively, mainly 2D plots

    if skipPlots:
        return

    ### [2/2] plot energy scale and resolution in 1D (the second dimension is used as intervals)
    # gets a graph projection of an ND histogram for a given axis
    # with axis index (ie x,y,z = 0,1,2) and other dimensions ind
    # new: to show plots like z peak vs phi for both charge + and - together, we draw them on same canvases now!
    def get_graph(hist, axis, axis_ind, *ind):
        ind = list(ind)
        ni = axis.GetNbins()
        xvals = [axis.GetBinCenter(i+1) for i in range(ni)]
        xvals_errLow = [xvals[i]-axis.GetBinLowEdge(i+1) for i in range(ni)]
        xvals_errHigh = [axis.GetBinUpEdge(i+1)-xvals[i] for i in range(ni)]
        yvals = [
            hist.GetBinContent(
                *ind[:axis_ind]
                + [i+1]
                + ind[axis_ind:]
            ) for i in range(ni)]
        yvals_err = [
            hist.GetBinError(
                *ind[:axis_ind]
                + [i+1]
                + ind[axis_ind:]
            ) for i in range(ni)]
        graph = ROOT.TGraphAsymmErrors(
            ni,
            array('d', xvals),
            array('d', yvals),
            array('d', xvals_errLow),
            array('d', xvals_errHigh),
            array('d', yvals_err),
            array('d', yvals_err),
        )
        return graph

    # some default colors for plots
    colors = [ROOT.kBlack, ROOT.kBlue, ROOT.kRed, ROOT.kGreen+2,
              ROOT.kMagenta+1, ROOT.kOrange+1, ROOT.kTeal-1,
              ROOT.kRed-3, ROOT.kCyan+2,
              ROOT.kBlue-3, ROOT.kOrange+5, ROOT.kBlack-3, ROOT.kGreen+5, ROOT.kCyan]

    def plot_1d_eff(savename, graphs,
                    labels=['Data', 'Simulation'],
                    colors=colors,
                    xlabel='', ylabel='Mass peak',
                    xRange=[], additional_text=[], etype='escale'):
        ng = len(graphs)
        mg = ROOT.TMultiGraph()
        for gi in range(ng):
            graphs[gi].SetLineColor(colors[gi])
            graphs[gi].SetMarkerColor(colors[gi])
            mg.Add(graphs[gi])

        canvas = ROOT.TCanvas(savename, savename, 800, 800)
        mg.Draw('AP0')
        mg.GetXaxis().SetTitle(xlabel)
        if xRange:
            mg.GetXaxis().SetLimits(*xRange)
            mg.GetXaxis().SetRangeUser(*xRange)
        mg.GetYaxis().SetTitle(ylabel)
        if etype == 'escale':
            mg.GetYaxis().SetRangeUser(88, 93)
        elif etype == 'ereso':
            mg.GetYaxis().SetRangeUser(0.0, 3.0)
        else:
            pass
        legend = ROOT.TLegend(0.5, 0.17, 0.92, 0.32)
        legend.SetTextFont(42)
        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        for gi in range(ng):
            legend.AddEntry(graphs[gi], labels[gi], 'l')
        legend.SetHeader('{} / {}'.format(num, denom))
        legend.Draw()

        if additional_text:
            nother = len(additional_text)
            dims = [0.18, 0.84-nother*0.04-0.02, 0.35, 0.84]
            text = ROOT.TPaveText(*dims+['NB NDC'])
            text.SetTextFont(42)
            text.SetBorderSize(0)
            text.SetFillColor(0)
            text.SetTextAlign(11)
            text.SetTextSize(0.03)
            for rtext in additional_text:
                text.AddText(rtext)
            text.Draw()

        CMS_lumi.cmsText = 'CMS'
        CMS_lumi.writeExtraText = True
        CMS_lumi.extraText = 'Preliminary'
        CMS_lumi.lumi_13TeV = "%0.1f fb^{-1}" % (lumi)
        CMS_lumi.CMS_lumi(canvas, 4, 11)

        canvas.Modified()
        canvas.Update()
        canvas.Print('{}.png'.format(savename))
        canvas.Print('{}.pdf'.format(savename))
        canvas.Print('{}.root'.format(savename))

        # save each graph
        tfile = ROOT.TFile('{}.root'.format(savename), 'update')
        for gi in range(ng):
            graphs[gi].SetTitle(labels[gi])
            graphs[gi].Write('g_{}_{}'.format(gi, labels[gi]))
        tfile.Close()

    # enumerate over the axis/variable to plot
    axes = [hists[extEffName].GetXaxis(),
            hists[extEffName].GetYaxis(),
            hists[extEffName].GetZaxis()]
    for vi, variableLabel in enumerate(variableLabels):

        # iterate over the other axis indices
        otherVariableLabels = [ovl for ovl in variableLabels
                               if ovl != variableLabel]
        otherVariableIndices = [ovi for ovi, ovl in enumerate(variableLabels)
                                if ovl != variableLabel]
        indices = [list(range(1, len(binning[vl])))
                   for vl in otherVariableLabels]
        if indices:
            graph_list = []
            label_list = []
            for index in itertools.product(*indices):
                graph_list.append( get_graph(hists[extEffName+'_'+etype+'Data'],
                                       axes[vi], vi, *index) )
                graph_list.append( get_graph(hists[extEffName+'_'+etype+'MC'],
                                     axes[vi], vi, *index) )
                xlabel = get_variable_name_pretty(variableLabel)
                ylabel = etitle
                xRange = [axes[vi].GetBinLowEdge(1),
                          axes[vi].GetBinUpEdge(axes[vi].GetNbins())]
                additional_text = []
                for novi, (ovi, ovl) in enumerate(zip(otherVariableIndices,
                                                      otherVariableLabels)):
                    xlow = axes[ovi].GetBinLowEdge(index[novi])
                    xhigh = axes[ovi].GetBinUpEdge(index[novi])
                    rtext = '{} < {} < {}'.format(
                        xlow, get_variable_name_pretty(ovl), xhigh)
                    additional_text += [rtext]
                label_list.append('Data {0}'.format( ' '.join(additional_text) ))
                label_list.append('MC {0}'.format( ' '.join(additional_text) ))
                #otherVariableLabel = get_bin_name(otherVariableLabels, index)
            plotDir = os.path.join(baseDir, 'plots',
                                   particle, probe,
                                   resonance, era,
                                   effName, etype+'_1d')
            os.makedirs(plotDir, exist_ok=True)
            plotName = '{}_vs_{}'.format(effName, variableLabel)
            plotPath = os.path.join(plotDir, plotName)
            plot_1d_eff(plotPath, graph_list, labels=label_list,
                        xlabel=xlabel, ylabel=ylabel,
                        xRange=xRange, etype=etype)

                ## dataEfficiency systs
                #graphs = [get_graph(hists[extEffName+'_'+etype+'Data'],
                #                    axes[vi], vi, *index)]
                #labels = ['Nominal']
                #for iSyst in itertools.chain(
                #        systList['dataEff']['fitTypes'],
                #        systList['dataEff']['shiftTypes']):
                #    graphs += [get_graph(
                #        hists[extEffName+'_'+etype+'Data_'+iSyst],
                #        axes[vi], vi, *index)]
                #    labels += [iSyst]
                #plotName = ('{}_{}_vs_{}_'+etype+'Data_syst').format(
                #    effName,
                #    otherVariableLabel,
                #    variableLabel,
                #)
                #plotPath = os.path.join(plotDir, plotName)
                #plot_1d_eff(plotPath, graphs,
                #            labels=labels,
                #            xlabel=xlabel, ylabel=ylabel,
                #            xRange=xRange, additional_text=additional_text, etype=etype)

                ## mcEfficiency systs
                #graphs = [get_graph(hists[extEffName+'_'+etype+'MC'],
                #                    axes[vi], vi, *index)]
                #labels = ['Nominal']
                #for iSyst in itertools.chain(
                #        systList['mcEff']['fitTypes'],
                #        systList['mcEff']['shiftTypes']):
                #    graphs += [get_graph(
                #        hists[extEffName+'_'+etype+'MC_'+iSyst],
                #        axes[vi], vi, *index)]
                #    labels += [iSyst]
                #plotName = ('{}_{}_vs_{}_'+etype+'MC_syst').format(
                #    effName,
                #    otherVariableLabel,
                #    variableLabel,
                #)
                #plotPath = os.path.join(plotDir, plotName)
                #plot_1d_eff(plotPath, graphs,
                #            labels=labels,
                #            xlabel=xlabel, ylabel=ylabel,
                #            xRange=xRange, additional_text=additional_text, etype=etype)

        # if no indices, easier, just itself
        else:
            graph_data = get_graph(hists[extEffName+'_'+etype+'Data'],
                                   axes[vi], vi)
            graph_mc = get_graph(hists[extEffName+'_'+etype+'MC'],
                                 axes[vi], vi)

            xlabel = get_variable_name_pretty(variableLabel)
            ylabel = etitle
            xRange = [axes[0].GetBinLowEdge(1),
                      axes[0].GetBinUpEdge(axes[0].GetNbins())]
            plotDir = os.path.join(baseDir, 'plots',
                                   particle, probe,
                                   resonance, era,
                                   effName, etype+'_1d')
            os.makedirs(plotDir, exist_ok=True)
            plotName = '{}_vs_{}'.format(effName, variableLabel)
            plotPath = os.path.join(plotDir, plotName)
            plot_1d_eff(plotPath, [graph_data, graph_mc],
                        xlabel=xlabel, ylabel=ylabel,
                        xRange=xRange, etype=etype)

def prepare_scalreso(baseDir, particle, probe, resonance, era,
            config, num, denom, variableLabels, lumi,
            skipPlots=False, cutAndCount=False):

    prepare(baseDir, particle, probe, resonance, era,
           config, num, denom, variableLabels, lumi,
           skipPlots, cutAndCount, etype='escale')

    prepare(baseDir, particle, probe, resonance, era,
           config, num, denom, variableLabels, lumi,
           skipPlots, cutAndCount, etype='ereso')

def build_prepare_jobs(particle, probe, resonance, era,
                       config, **kwargs):
    _baseDir = kwargs.pop('baseDir', '')
    _numerator = kwargs.pop('numerator', [])
    _denominator = kwargs.pop('denominator', [])
    _registry = kwargs.pop('registry', None)

    if _registry is not None:
        registry.reset()
        registry.load_json(_registry)

    subEra = era.split('_')[0]  # data subera is beginning of era
    lumi = registry.luminosity(particle, probe, resonance, era, subEra, **kwargs)

    jobs = []
    # iterate through the efficiencies
    efficiencies = config.efficiencies()
    for num, denom in efficiencies:
        if _numerator and num not in _numerator:
            continue
        if _denominator and denom not in _denominator:
            continue

        # iterate through the output binning structure
        for variableLabels in config.binVariables():

            jobs += [[_baseDir, particle, probe, resonance, era,
                     config, num, denom, tuple(variableLabels), lumi]]

    return jobs
